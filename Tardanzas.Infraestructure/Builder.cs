﻿using Tardanzas.Data.EF;
using Tardanzas.Logic;
using System;

namespace Tardanzas.Infraestructure
{
    public class Builder
    {

        public static EmployeeL EmployeeL()
        {
            return new EmployeeL(new EFTardanzasRepository());
        }

        public static DelayL DelayL()
        {
            return new DelayL(new EFTardanzasRepository());
        }

    }
}
