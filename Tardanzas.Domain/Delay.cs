namespace Tardanzas.Domain
{
    using System;

    public partial class Delay
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public DateTime CurrentDate { get; set; }

        public int Tax { get; set; }

        public DateTime? CreatedAt { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public int? CreatedBy { get; set; }

        public int? ModifiedBy { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
