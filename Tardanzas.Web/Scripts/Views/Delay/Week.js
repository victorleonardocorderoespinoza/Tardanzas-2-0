﻿; (function () {

    page.base('/delay/week');

    page('/', index);
    page('/:year/:week', other);
    page();

    function index() {
        generate(moment().year(), moment().week());
    }

    function other(ctx) {
        generate(ctx.params.year, ctx.params.week);
    }

    $('.dropdown-toggle').dropdown();

    $.post("/Delay/WeekList", { year: moment().year() }, function (data) {

        $.each(data, function (index, item) {
            $("#weeks").append($("<li>").append($("<a>", { href: "/delay/week/" + moment().year() + "/" + item.Id, text: item.WeekYear })));
        });
    })
    .fail(function () { alert("Error") });

    var table_delay = $('#table-delay').DataTable({
        fixedColumns: true,
        dom: 'Bfrtip',
        paging: false,
        searching: false,
        columns: [
                { "data": "Employee" },
                { "data": "Monday" },
                { "data": "Tuesday" },
                { "data": "Wednesday" },
                { "data": "Thursday" },
                { "data": "Friday" },
                { "data": "Total" }
        ]
    });

    var table_total = $('#table-total').DataTable({
        fixedColumns: true,
        dom: 'Bfrtip',
        paging: false,
        searching: false,
        columns: [
                { "data": "Period" },
                { "data": "PeriodDay" },
                { "data": "Total" }
        ]
    });

    function generate(year, week) {

        $.post("/Delay/DelayCurrentWeekTotal", { year: year, week: week }, function (data) {

            if (data.length == 0) return;

            table_total.clear().draw();
            table_total.rows.add(data).draw();

            $("#total-week").text();

            $("#graph_total").empty();
            Morris.Bar({
                element: 'graph_total',
                data: data,
                xkey: 'PeriodDay',
                ykeys: ['Total'],
                labels: ['Total'],
                xLabelAngle: 30,
                hideHover: 'auto',
                parseTime: false,
                resize: true
            });

        })
        .fail(function () { alert("Error") })

        $.post("/Delay/DelayCurrentWeek", { year: year, week: week }, function (data) {

            table_delay.clear().draw();
            table_delay.rows.add(data).draw();

            $("#graph_delay").empty();
            $("#legend").empty();

            var line = Morris.Line({
                element: 'graph_delay',
                data: data,
                xkey: 'Employee',
                ykeys: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
                labels: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'],
                xLabelAngle: 10,
                hideHover: 'auto',
                parseTime: false,
                resize: true
            });

            line.options.labels.forEach(function (label, i) {
                console.log(label);
                var item = $('<span></span>').text(label).css('color', line.options.lineColors[i])
                $('#legend').append(item)
            })

            $.post("/Delay/PreviusLatecomers", { year: year, week: week }, function (data) {

                $.each(data, function (row, employee) {
                    $.each($("#table-delay>tbody>tr"), function (index, item) {
                        if (employee.Name === $(item).find('td').first().text()) {
                            $(item).addClass("danger");
                        }
                    });
                });

            })
            .fail(function () { alert("Error") })

        })
        .fail(function () { alert("Error") })


    }


})()