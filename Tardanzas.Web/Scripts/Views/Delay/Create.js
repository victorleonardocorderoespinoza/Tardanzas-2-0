﻿; (function () {


    var emp;
     
    $.post("/Employee/EmployeeList", function (data) {
        emp = data;

        $("#tbl-data>thead").append($("<tr>")
        .append($("<td>", { text: "Id" }))
        .append($("<td>", { text: "Colaborador" })))

        $.each(emp, function (index, item) {
            $("#tbl-data>tbody").append($("<tr>")
                .append($("<td>", { text: item.Id }))
                .append($("<td>", { text: item.Name })))
        });
    })
    .fail(function () { alert("Error") })

    $("#ta-data").on("change", function (e) {

        var data = $("#ta-data").val();

        $("#tbl-data>thead").empty();
        $("#tbl-data>tbody").empty();

        $("#tbl-data>thead").append($("<tr>")
            .append($("<td>", { text: "Id" }))
            .append($("<td>", { text: "Colaborador" })))

        $.each(emp, function (index, item) {
            $("#tbl-data>tbody").append($("<tr>")
                .append($("<td>", { text: item.Id }))
                .append($("<td>", { text: item.Name })))
        });

        $.each(data.split("\n"), function (index, item) {

            $.each(item.split("\t"), function (index_col, item_col) {
                if (index == 0) {
                    $("#tbl-data>thead>tr").append($("<td>", { text: item_col }))
                } else {
                    $("#tbl-data>tbody>tr").eq(index - 1).append($("<td>", { class: 'tax', text: item_col }))
                }
            });

        });
    });

    $("#btn-save").on("click", function (e) {
         
        $.each($("#tbl-data>tbody>tr"), function (index, item) {

            $.each($(item).find('.tax'), function (index_col, item_col) {

                var date = $("#tbl-data>thead>tr>td").eq(index_col + 2).text().split('-');

                var delay = {
                    EmployeeId: $(item).find('td').eq(0).text(),
                    Day: date[0],
                    Month: date[1],
                    Year: $("#s-year").val(),
                    Tax: $(item_col).text()
                }

                if ($.trim($(item_col).text()) == "") {
                    proc = "DeleteDelay";
                } else {
                    proc = "InsertDelay";
                }

                $.post("/Delay/" + proc, $.param(delay), function (data) {
                    if (data) {
                        $(item_col).addClass("success");
                    } else {
                        $(item_col).addClass("danger");
                    }
                     
                    if (index_col === $(item).find('.tax').length - 1) {
                         
                    }
                })
                .fail(function () { alert("Error") })
                
            });
        });
    });
})()

