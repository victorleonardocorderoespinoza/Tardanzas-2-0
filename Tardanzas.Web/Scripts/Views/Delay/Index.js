﻿; (function () {

 

    $.post("/Delay/WeekList", { year: moment().year() }, function (data) {

        $.each(data, function (index, item) {
            $("#weeks").append($("<li>").append($("<a>", { href: "/delay/week/" + moment().year() + "/" + item.Id, text: item.WeekYear })));
        });
    })
    .fail(function () { alert("Error") });

    generate(2016, 20);

    function generate(year, week) {

        $.post("/Delay/DelayWeekDay", { year: year, week: week }, function (data) {

            $("#graph_week_day").empty();
            $("#legend").empty();

            var line = Morris.Line({
                element: 'graph_week_day',
                data: data,
                xkey: 'WeekName',
                ykeys: ['Monday', 'Tuesday', 'Wednesday', 'Thrusday', 'Friday', 'Average'],
                labels: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Promedio'],
                hideHover: 'auto',
                parseTime: false,
                resize: true
            });

            line.options.labels.forEach(function (label, i) {
                console.log(label);
                var item = $('<span></span>').text(label).css('color', line.options.lineColors[i])
                $('#legend').append(item)
            })

        })
        .fail(function () { alert("Error") })


    }


})()