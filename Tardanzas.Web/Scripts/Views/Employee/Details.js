﻿; (function () {

    page.base('/employee/details');

    page('/', index);
    page('/:employee', other);
    page();

    function index() {

    }

    function other(ctx) {
        generate(ctx.params.employee);
    }

    $('.dropdown-toggle').dropdown();

    $.post("/Employee/EmployeeList", function (data) {

        $.each(data, function (index, item) {
            $("#employees").append($("<li>").append($("<a>", { href: "/employee/details/" + item.Slug, text: item.Name })));
        });
    });

    var table_delay = $('#table-delay').DataTable({
        fixedColumns: true,
        dom: 'Bfrtip',
        paging: false,
        searching: false,
        columns: [
                { "data": "WeekYear" },
                { "data": "Monday" },
                { "data": "Tuesday" },
                { "data": "Wednesday" },
                { "data": "Thursday" },
                { "data": "Friday" },
                { "data": "Previous" },
                { "data": "Total" },
                {
                    "data": "DiffPercent",
                    render: function (data, type, row) {
                        if (data > 0) {
                            return "<span class='glyphicon glyphicon-arrow-up text-success'></span> " + data + "%"
                        } else if (data < 0) {
                            return "<span class='glyphicon glyphicon-arrow-down text-danger'></span> " + data + "%"
                        } else {
                            return "<span class='glyphicon glyphicon-arrow-right'></span> " + data + "%"
                        }
                    }
                }
        ]
    });
     

    function generate(slug) {

        $.post("/Employee/WeekDiference", { slug: slug }, function (data) {

            if (data.length == 0) return;

            table_delay.clear().draw();
            table_delay.rows.add(data).draw();
 
        })
        .fail(function () { alert("Error en Gráfico") })
 
    }


})()