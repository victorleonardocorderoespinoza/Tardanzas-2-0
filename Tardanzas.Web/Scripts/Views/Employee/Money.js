﻿; (function () {


    $('.dropdown-toggle').dropdown();

    $.post("/Employee/EmployeeList", function (data) {

        $.each(data, function (index, item) {
            $("#employees").append($("<li>").append($("<a>", { href: "/employee/details/" + item.Slug, text: item.Name })));
        });
    });

    var table_delay = $('#table-delay').DataTable({
        fixedColumns: true,
        dom: 'Bfrtip',
        paging: false,
        searching: false,
        columns: [
                { "data": "Id" },
                { "data": "Name" },
                { "data": "Subtotal" },
                { "data": "Penalty" },
                { "data": "Total" },
                {
                    "data": null,
                    render: function (data, type, row) {
                        return "<a><span class='glyphicon glyphicon-plus-sign'></span></a>";
                    }
                }
        ],
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            subtotal = api
                .column(2)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            penalty = api
               .column(3)
               .data()
               .reduce(function (a, b) {
                   return intVal(a) + intVal(b);
               }, 0);

            total = api
               .column(4)
               .data()
               .reduce(function (a, b) {
                   return intVal(a) + intVal(b);
               }, 0);


            // Update footer

            $(api.column(2).footer()).html(subtotal);
            $(api.column(3).footer()).html(penalty);
            $(api.column(4).footer()).html(total);
        }
    });

    $.post("/Employee/GetConsolidatedGeneralTax", function (data) {

        console.log(data);
        if (data.length == 0) return;

        table_delay.clear().draw();
        table_delay.rows.add(data).draw();

    })
    .fail(function () { })


})()