﻿; (function () {


    function update() {
        $('#clock').html(moment().format('DD MMMM YYYY H:mm:ss'));
    }

    setInterval(update, 1000);


})()