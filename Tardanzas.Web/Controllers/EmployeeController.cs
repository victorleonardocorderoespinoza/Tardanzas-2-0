﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Tardanzas.Infraestructure;
using Tardanzas.Logic;
using Tardanzas.Logic.Models;

namespace Tardanzas.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly EmployeeL employeel;
        private readonly DelayL delayl;

        public EmployeeController()
        {
            employeel = Builder.EmployeeL();
            delayl = Builder.DelayL();
        }

        [HttpPost]
        public JsonResult EmployeeList()
        {
            return Json(employeel.GetAll());
        }

        [HttpPost]
        public JsonResult WeekDiference(string slug)
        {
            return Json(employeel.GetEmployeeWeekDiffList(slug));
        }

        [HttpPost]
        public JsonResult CreateDelay(CreateDelayModel delay)
        {
            return Json(employeel.Insert(delay));
        }

        [HttpPost]
        public JsonResult GetConsolidatedGeneralTax()
        {
            return Json(employeel.GetConsolidatedGeneral());
        }

        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }

        // GET: Employee/Details/5
        public ActionResult Details()
        {
            return View();
        }

        public ActionResult Money()
        {
            return View();
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
