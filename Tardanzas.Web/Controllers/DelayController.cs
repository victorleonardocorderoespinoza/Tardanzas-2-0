﻿using System.Web.Mvc;
using Tardanzas.Logic.Models;
using Tardanzas.Logic;
using Tardanzas.Infraestructure;
using System;

namespace Tardanzas.Web.Controllers
{
    public class DelayController : Controller
    {

        private readonly EmployeeL employeel;
        private readonly DelayL delayl;

        public DelayController()
        {
            employeel = Builder.EmployeeL();
            delayl = Builder.DelayL();
        }

        #region Services

        [HttpPost]
        public JsonResult WeekList(int year)
        {
            return Json(delayl.GetDelayWeekList(year));
        }

        [HttpPost]
        public JsonResult PreviusLatecomers(int year, int week)
        {
            return Json(delayl.PreviusLatecomers(year, week));
        }

        [HttpPost]
        public JsonResult DelayCurrentWeek(int year, int week)
        {
            return Json(delayl.GetDelayCurrentWeek(year, week));
        }

        [HttpPost]
        public JsonResult DelayCurrentWeekTotal(int year, int week)
        {
            return Json(delayl.GetDelayCurrentWeekTotal(year, week));
        }

        [HttpPost]
        public JsonResult DelayWeekDay(int year, int week)
        {
            return Json(delayl.GetDelayWeekDay(year, week));
        }

        [HttpPost]
        public JsonResult InsertDelay(CreateDelayModel delay)
        {
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    return Json(false);
                }
            }
            return Json(employeel.Insert(delay));
        }

        [HttpPost]
        public JsonResult DeleteDelay(DeleteDelayModel delay)
        {
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    return Json(false);
                }
            }
            return Json(employeel.Delete(delay));
        }

        #endregion 

        // GET: Delay
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Week()
        {
            return View();
        }

        public ActionResult Month()
        {
            return View();
        }


        // GET: Delay/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Delay/Create
        public ActionResult Create()
        {
            var model = new CreateDelayModel();
            return View(model);
        }

        // POST: Delay/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Import()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Import(int id, FormCollection collection)
        {
            return View();
        }

        // GET: Delay/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Delay/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Delay/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Delay/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
