﻿using Insight.Database;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tardanzas.Domain;

namespace Tardanzas.Data.InsightDB
{
    public class TardanzasDB
    {
        private IDbConnection db;

        public TardanzasDB(string connectionStringName)
        {
            db = new SqlConnection(
                ConfigurationManager
                .ConnectionStrings[connectionStringName]
                .ConnectionString);
        }

        public void Dispose()
        {
            if (db.State == ConnectionState.Open) db.Close();
            db.Dispose();
            db = null;
        }

        public IEnumerable<TResult> ExecuteQuery<TResult>(string query, object parameters)
        {
            return db.QuerySql<TResult>(query, parameters);
        }

        public IEnumerable<TResult> ExecuteProcedure<TResult>(string procedure, object parameters)
        {
            return db.Query<TResult>(procedure, parameters);
        }

        public TResult Insert<TResult>(string procedure, TResult entity)
        {
            return db.Insert(procedure, entity);
        }


    }
}
