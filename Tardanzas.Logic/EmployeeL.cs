﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tardanzas.Data.InsightDB;
using Tardanzas.Data.Repository;
using Tardanzas.Logic.Models;

namespace Tardanzas.Logic
{
    public class EmployeeL
    {

        private readonly ITardanzasRepository db;
        private readonly TardanzasDB indb;

        public EmployeeL(ITardanzasRepository repositorio)
        {
            db = repositorio;
            indb = new TardanzasDB("Tardanzas");
        }

        public IEnumerable<EmployeeModel> GetAll()
        {
            return db.Employee.GetAll()
            .Select(o => new EmployeeModel
            {
                Id = o.Id,
                Name = o.Name,
                Slug = o.Slug
            }).ToList();
        }

        public IEnumerable<EmployeeWeekDiff> GetEmployeeWeekDiffList(string slug)
        {
            return indb.ExecuteProcedure<EmployeeWeekDiff>("report_employee_month_diference", new { slug = slug });
        }

        public IEnumerable<ConsolidatedGeneralTaxModel> GetConsolidatedGeneral()
        {
            return indb.ExecuteProcedure<ConsolidatedGeneralTaxModel>("get_consolidated_general_employee_tax", new { });
        }


        public bool Insert(CreateDelayModel delay)
        {
            if (delay.CurrentDate == null)
            {
                return false;
            }

            var ud = db.Delay.SingleOrDefault(x => x.CurrentDate == delay.CurrentDate.Value && x.EmployeeId == delay.EmployeeId);

            if (ud != null)
            {
                ud.Tax = delay.Tax.Value;
                ud.ModifiedAt = DateTime.Now;
            }
            else {

                db.Delay.Add(new Domain.Delay()
                {
                    EmployeeId = delay.EmployeeId,
                    CurrentDate = delay.CurrentDate.Value,
                    Tax = delay.Tax.Value,
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now
                });
            }

            db.Commit();
            return true;
        }

        public object Delete(DeleteDelayModel delay)
        {
            if (delay.CurrentDate == null)
            {
                return false;
            }

            var ud = db.Delay.SingleOrDefault(x => x.CurrentDate == delay.CurrentDate.Value && x.EmployeeId == delay.EmployeeId);

            if (ud != null)
            {
                db.Delay.Delete(ud);
                db.Commit();
                return true;
            }
            else {
                return false;
            }
        }
    }
}
