﻿using System.Collections.Generic;
using Tardanzas.Data.Repository;
using Tardanzas.Data.InsightDB;
using Tardanzas.Logic.Models;
using System;

namespace Tardanzas.Logic
{
    public class DelayL
    {

        private readonly TardanzasDB indb;
        private readonly ITardanzasRepository efdb;

        public DelayL(ITardanzasRepository repositorio)
        {
            efdb = repositorio;
            indb = new TardanzasDB("Tardanzas");
        }

        public IEnumerable<DelayWeekModel> GetDelayCurrentWeek(int year, int week)
        {
            return indb.ExecuteProcedure<DelayWeekModel>("report_delay_week", new { year = year, week = week });
        }

        public IEnumerable<DelayWeekTotalModel> GetDelayCurrentWeekTotal(int year, int week)
        {
            return indb.ExecuteProcedure<DelayWeekTotalModel>("graph_delay_week", new { year = year, week = week });
        }

        public object PreviusLatecomers(int year, int week)
        {
            return indb.ExecuteProcedure<PreviousLatecomer>("get_delay_previous_latecomers_list", new { year = year, week = week });
        }

        public IEnumerable<WeekModel> GetDelayWeekList(int year)
        {
            return indb.ExecuteProcedure<WeekModel>("get_delay_weeks_list", new { year = year });
        }

        public IEnumerable<WeekDayDelayModel> GetDelayWeekDay(int year, int week)
        {
            return indb.ExecuteProcedure<WeekDayDelayModel>("graph_delay_week_day", new { year = year, week = week });
        }
    }
}
