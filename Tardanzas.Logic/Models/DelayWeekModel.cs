﻿namespace Tardanzas.Logic.Models
{
    public class DelayWeekModel
    {

        public int IdEmployee { get; set; }
        public string Employee { get; set; }

        public int? Monday { get; set; }
        public int? Tuesday { get; set; }
        public int? Wednesday { get; set; }
        public int? Thursday { get; set; }
        public int? Friday { get; set; }

        public int? Total { get; set; }
    }
}
