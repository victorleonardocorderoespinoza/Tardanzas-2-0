﻿namespace Tardanzas.Logic.Models
{
    public class EmployeeWeekDiff
    {

        public int WeekYear { get; set; }
        public int? Monday { get; set; }
        public int? Tuesday { get; set; }
        public int? Wednesday { get; set; }
        public int? Thursday { get; set; }
        public int? Friday { get; set; }
        public int Previous { get; set; }
        public int Total { get; set; }
        public double DiffPercent { get; set; }

    }
}
