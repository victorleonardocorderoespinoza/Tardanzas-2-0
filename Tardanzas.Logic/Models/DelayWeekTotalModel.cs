﻿namespace Tardanzas.Logic.Models
{
    public class DelayWeekTotalModel
    {

        public string Period { get; set; }
        public string PeriodDay { get; set; }
        public int Total { get; set; }

    }
}
