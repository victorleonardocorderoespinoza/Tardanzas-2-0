﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tardanzas.Logic.Models
{
    public class WeekDayDelayModel
    {

        public int WeekD { get; set; }
        public string WeekName { get { return "Sem-" + WeekD; } }

        public int? Monday { get; set; }
        public int? Tuesday { get; set; }
        public int? Wednesday { get; set; }
        public int? Thrusday { get; set; }
        public int? Friday { get; set; }

        public int Total { get; set; }
        public float Average { get; set; }

    }
}
