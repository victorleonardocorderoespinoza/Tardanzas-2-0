﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tardanzas.Logic.Models
{
    public class ConsolidatedGeneralTaxModel
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public int Total { get; set; }
        public int Subtotal { get; set; }
        public int Penalty { get; set; }

    }
}
