﻿namespace Tardanzas.Logic.Models
{
    public class PreviousLatecomer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Total { get; set; }
    }
}
