﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Tardanzas.Logic.Models
{
    public class CreateDelayModel
    {

        public CreateDelayModel()
        {
            Months["ene"] = 1;
            Months["feb"] = 2;
            Months["mar"] = 3;
            Months["abr"] = 4;
            Months["may"] = 5;
            Months["jun"] = 6;
            Months["jul"] = 7;
            Months["ago"] = 8;
            Months["set"] = 9;
            Months["oct"] = 10;
            Months["nov"] = 11;
            Months["dic"] = 12;
        }

        [Required]
        public int Year { get; set; }

        [Required]
        [MaxLength(3), MinLength(3)]
        public string Month { get; set; }

        [Required]
        public int Day { get; set; }

        public DateTime? CurrentDate
        {
            get
            {
                DateTime cdate;
                DateTime.TryParse(string.Format("{0}/{1}/{2}", Year, Months[Month], Day), out cdate);
                return cdate;
            }
        }

        [Required]
        public int EmployeeId { get; set; }
        [Required]
        public int? Tax { get; set; }

        Dictionary<string, int> Months = new Dictionary<string, int>();

    }
}
