﻿namespace Tardanzas.Logic.Models
{
    public class EmployeeModel
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }

    }
}
