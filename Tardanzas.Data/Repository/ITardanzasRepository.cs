﻿using System;
using System.Collections.Generic;
using Tardanzas.Domain;

namespace Tardanzas.Data.Repository
{
    public interface ITardanzasRepository : IDisposable
    {
        IGenericRepository<Employee> Employee { get; }
        IGenericRepository<Delay> Delay { get; }

        void Commit();
    }
}