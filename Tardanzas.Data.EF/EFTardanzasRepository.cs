﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Tardanzas.Data.EF.RepositoryEntities;
using Tardanzas.Domain;
using Tardanzas.Data.Repository;
using System.Collections.Generic;

namespace Tardanzas.Data.EF
{
    public class EFTardanzasRepository  : DbContext, ITardanzasRepository
    {

        private readonly IGenericRepository<Employee> _employeer;
        private readonly IGenericRepository<Delay> _delayr;

        public EFTardanzasRepository()
            : base("name=Tardanzas")
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;

            _employeer = new EmployeeRepository(this);
            _delayr = new DelayRepository(this);
        }

        #region 

        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Delay> Delay { get; set; }

        IGenericRepository<Employee> ITardanzasRepository.Employee
        {
            get
            {
                return _employeer;
            }
        }

        IGenericRepository<Delay> ITardanzasRepository.Delay
        {
            get
            {
                return _delayr;
            }
        }

        #endregion


        #region DbContext

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Employee>() 
                .Property(e => e.Name)
                .IsUnicode(false);
        }

        #endregion
        #region IUnitOfWork Implementation

        public void Commit()
        {
            this.SaveChanges();
        }

        #endregion

    }
}