﻿using Tardanzas.Domain;

namespace Tardanzas.Data.EF.RepositoryEntities
{
    class EmployeeRepository : EFGenericRepository<EFTardanzasRepository, Employee>
    {
        public EmployeeRepository(EFTardanzasRepository contexto)
            : base(contexto)
        {

        }
    }
}
