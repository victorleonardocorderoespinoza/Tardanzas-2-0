﻿using Tardanzas.Domain;

namespace Tardanzas.Data.EF.RepositoryEntities
{
    class DelayRepository : EFGenericRepository<EFTardanzasRepository, Delay>
    {
        public DelayRepository(EFTardanzasRepository contexto)
            : base(contexto)
        {

        }
    }
}